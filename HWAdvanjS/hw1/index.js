// Створити клас Employee, у якому будуть такі характеристики - name (ім'я), age (вік), salary (зарплата). Зробіть так, щоб ці характеристики заповнювалися під час створення об'єкта.
// Створіть гетери та сеттери для цих властивостей.
// Створіть клас Programmer, який успадковуватиметься від класу Employee, і який матиме властивість lang (список мов).
// Для класу Programmer перезапишіть гетер для властивості salary. Нехай він повертає властивість salary, помножену на 3.
// Створіть кілька екземплярів об'єкта Programmer, виведіть їх у консоль.
// Поясніть своїми словами, як ви розумієте, як працює прототипне наслідування в Javascript
// Шаблон на звертаючись до якого ми можимо отримати його наповнення,властивості
// Для чого потрібно викликати super() у конструкторі класу-нащадка?Щоби отримати властивості батьківського класу.
class Employee {
  constructor(name = "Myname", age = "00", salary = "000") {
    this._name = name;
    this._age = age;
    this._salary = salary;
  }
  get name() {
    return this._name;
  }
  get age() {
    return this._age;
  }
  get salary() {
    return this._salary;
  }
  set name(myname) {
    this._name = myname;
  }
  set age(myage) {
    this._age = myage;
  }
  set salary(mysalary) {
    this._salary = mysalary;
  }
}
const worker1 = new Employee("denys", 12, 5000);

const w2 = new Employee();

class Programmer extends Employee {
  constructor(name, age, salary, lang) {
    super(name, age, salary);
    this.lang = lang;
  }
  get salary() {
    return this._salary * 3;
  }
}
const p1 = new Programmer("Ol", 12, 3200, "russian");
const p2 = new Programmer("Kate", 18, 4200, "eng");
const p3 = new Programmer("Michael", 26, 4500, "ua");
console.log(p1, p2, p3)
