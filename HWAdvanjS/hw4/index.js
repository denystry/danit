const URI = "https://ajax.test-danit.com/api/swapi/films";
const root = document.querySelector("#root");


class Movies {
  constructor(url, root) {
    this.url = url;
    this.root = root;

  }
  request(link) {
    return fetch(link, {
        method: "GET",
        headers: {
          'Content-Type': 'application/json',
        }
      })
      .then((response) => {

        if (!response.ok) {
          throw new Error(response);
        } else {
          return response.json();
        }
      })
      .catch(e => {
        console.log(e);
      })
  }
  render() {
    this.request(this.url).then((data) => {

      const moviesList = data.map(({
        episodeId,
        name,
        openingCrawl,
        characters
      }, index) => {
        let filmItem = document.createElement('li');
        let filmBlock = document.createElement('div');
        let filmList = document.createElement('ul');

        filmList.append(filmItem);
        filmItem.append(filmBlock);

        let filmTitleM = document.createElement('h2')
        let episodeIDM = document.createElement('p')
        let openingCrawlM = document.createElement('p')

        filmTitleM.innerText = `${name}`
        episodeIDM.innerText = `Episode ${episodeId}`
        openingCrawlM.innerText = `${openingCrawl}`

        filmBlock.append(filmTitleM, episodeIDM, openingCrawlM)

        characters.forEach(el => {
          let names = [];
          this.request(el).then(({
            name
          }) => {
            episodeIDM.before(`> ${name} |`)
          })
        })

        this.root.append(filmList);
      });
    })
  }
}
new Movies(URI, root).render();
