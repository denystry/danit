const checkBtn = document.createElement('button');
const container = document.querySelector('#root');
let info = document.createElement('span')
checkBtn.innerText = "GET IP INFO";
container.append(checkBtn);

checkBtn.addEventListener('click', checkIp)


async function checkIp() {
  let resIp = await fetch('https://api.ipify.org/?format=json')
  let data = await resIp.json();
  let {
    ip
  } = data;
  await getLoc();
  async function getLoc() {
    let respLoc = await fetch(`http://ip-api.com/json/${ip}?fields=continent,country,regionName,city,district&lang=ru`);
    let data = await respLoc.json();
    let {
      continent,
      country,
      regionName,
      city,
      district
    } = data;
    info.innerText = `Континент: ${continent}, Страна: ${country}, Город: ${city}, Регион: ${regionName}, Район:${district}`
    checkBtn.after(info)

  }
}
