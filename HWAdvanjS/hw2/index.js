// Наведіть кілька прикладів, коли доречно використовувати в коді конструкцію try...catch.
// 1. якщо ми можемо припустити що наш код має помилки.
// 2. записувати виявленні помилки в журнали подій ( наприклад на сервері)
// 3. дає можливість нам самим обробляти помилки а не браузеру
// 4. виявити єтапи місця в коді де відбувається помилка
// 5. Створювати свої власні повідомлення і назви помилок і записувати їх.


const books = [{
    author: "Люсі Фолі",
    name: "Список запрошених",
    price: 70
  },
  {
    author: "Сюзанна Кларк",
    name: "Джонатан Стрейндж і м-р Норрелл",
  },
  {
    name: "Дизайн. Книга для недизайнерів.",
    price: 70
  },
  {
    author: "Алан Мур",
    name: "Неономікон",
    price: 70
  },
  {
    author: "Террі Пратчетт",
    name: "Рухомі картинки",
    price: 40
  },
  {
    author: "Анґус Гайленд",
    name: "Коти в мистецтві",
  }
];

let customComponent = document.getElementById('root');
let list = document.createElement('ol');
customComponent.insertAdjacentElement('beforeend',list)
let mylist = document.querySelector('ol');

for (let i = 0; i < books.length; i++) {
  let book = books[i];


  try {
    if (!book.author) {
      throw new SyntaxError("Книга:" + i + " Дані неповні: немає автора");

    }
    if (!book.name) {
      throw new SyntaxError("Книга:" + i + " Дані неповні: немає імя");

    }
    if (!book.price) {
      throw new SyntaxError("Книга:" + i + " Дані неповні: немає ціни");

    }
    mylist.insertAdjacentHTML('beforeEnd',`<li>Author: ${book.author} Name: ${book.name} Price: ${book.price}</li>`)
  } catch (e) {
    console.log("Помилка:" + e.message);

  }


}
