class App {
  constructor() {
    this.app = document.querySelector('.app');
    this.tweet = document.createElement('div');

  }
  init() {

    this.tweet.classList.add('tweet');
    this.app.append(this.tweet);
    this.getCards()

  }
  getCards() {
    let cards = fetch('https://ajax.test-danit.com/api/json/posts')
      .then(response => response.json())
    let users = fetch('https://ajax.test-danit.com/api/json/users')
      .then(response => response.json())
    Promise.all([cards, users])
      .then(
        values => {

          const posts = values[0].map(post => {
            const users = values[1].find(user => {
              return user.id === post.userId
            })
            const firstName = users.name.split(' ')[0];
            const lastName = users.name.split(' ')[1];
            const email = users.email
            const userName = users.userName
            return {
              ...post,
              firstName,
              lastName,
              email,
              userName
            }
          })

          posts.forEach(e => new Card(e).initCards())
        },
        reason => {
          console.log(reason);
        });
  }
}

class Card extends App {
  constructor({
    title,
    body,
    userName,
    firstName,
    lastName,
    email,
    id,
    userId
  }) {
    super()
    this.title = title;
    this.description = body;
    this.userName = userName;
    this.firstName = firstName;
    this.lastName = lastName;
    this.userEmail = email;
    this.userId = userId;
    this.cardId = id;
    this.btnDelete = document.createElement('button');
    this.cardTitle = document.createElement('h2');
    this.cardDescription = document.createElement('p');
  }

  initCards() {
    this.createCard()
    this.deleteCard()
  }

  createCard() {
    this.cardTitle.innerText = this.title;
    this.cardTitle.classList.add('cardTitle')
    this.cardDescription.classList.add('description')
    this.cardDescription.innerText = this.description;
    let fullName = document.createElement('span');
    fullName.classList.add('fullname')
    fullName.innerText = this.firstName + " " + this.lastName;
    let userEmail = document.createElement('span');
    userEmail.classList.add('userEmail')
    userEmail.innerText = this.userEmail;
    this.btnDelete.classList.add('delete-btn')
    this.btnDelete.innerText = "Delete"
    this.renderCard(this.cardTitle, this.cardDescription, name, fullName, userEmail, this.btnDelete)
  }

  renderCard(cardTitle, cardDescription, name, fullName, userEmail, btnDelete) {
    const cardWrapper = document.createElement('div');
    cardWrapper.classList.add('card')
    cardWrapper.append(cardTitle, cardDescription, name, fullName, userEmail, btnDelete)
    document.querySelector('.tweet').prepend(cardWrapper)
  }

  deleteCard() {
    this.btnDelete.addEventListener('click', onRemove.bind(this))

    function onRemove() {
      fetch(`https://ajax.test-danit.com/api/json/posts/${this.cardId}`, {
        method: 'DELETE',
      })
      this.btnDelete.parentElement.remove()
      this.btnDelete.removeEventListener('click', onRemove.bind(this))
    }
  }
}

let app = new App()
app.init()
