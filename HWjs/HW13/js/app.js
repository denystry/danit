"use strict";
// 1. Опишите своими словами разницу между функциями `setTimeout()` и `setInterval()`.
//     сет таймуат запускает функцию через опреденненое время один раз, а интревал вызывает повторно через опреденненое время
// 2. Что произойдет, если в функцию `setTimeout()` передать нулевую задержку? Сработает ли она мгновенно, и почему?
    // JavaScript имеет один поток выполнения, который используется совместно с рендерингом страницы. По сути, запуск JavaScript блокирует обновление DOM.
    // Нулевая задержка запланирует асинхронный запуск обратного вызова после минимально возможной задержки, которая будет составлять около 10 мс, когда вкладка находится в фокусе, а поток выполнения JavaScript не занят.

// 3. Почему важно не забывать вызывать функцию `clearInterval()`, когда ранее созданный цикл запуска вам уже не нужен?
//   Очистка памяти, быстроедействие , нагрузка


let imgList = [...document.getElementsByClassName("image-to-show")];

document.getElementById("stop").addEventListener("click", function() {
  clearInterval(timer)
});

document.getElementById("continue").addEventListener("click", function() {
  bannerSlider()
});
let counter = 1;
let timer;
const bannerSlider = () => {

  const changer = () => {


    imgList.forEach((item) => {
      item.classList.remove("active")
    });
    if (counter < imgList.length) {
      imgList[counter].classList.add("active")
      counter++;
    }
    if (counter === (imgList.length)) {
      counter = 0;
    }
  }

  timer = setInterval(changer, 3000);
}
bannerSlider();
