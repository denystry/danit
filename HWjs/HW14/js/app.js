"use strict";


let changeTheme = document.getElementById("changetheme");


if (localStorage.getItem('themeDark') == "true") {
  document.body.style.backgroundColor = 'black';

}
 if (localStorage.getItem('themeDark') == "false") {
  document.body.style.backgroundColor = 'white';

}


const changeBg = () => {
  localStorage["themeDark"] == "true" ? localStorage["themeDark"]= "false" : localStorage["themeDark"]= "true";

  if (localStorage.getItem('themeDark') == "true") {
    document.body.style.backgroundColor = 'black';
  }
  if (localStorage.getItem('themeDark') == "false") {
    document.body.style.backgroundColor = 'white';

  }
}


changeTheme.addEventListener("click", changeBg)
