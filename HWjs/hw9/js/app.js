"use strict";
// Опишіть, як можна створити новий HTML тег на сторінці.
//   document.createElement("тега")
//
// Опишіть, що означає перший параметр функції insertAdjacentHTML і опишіть можливі варіанти цього параметра.
//
//   beforebegin - вставити елемент перед елементом переданим як другий параметр
//   afterbegin - вставити елемент перед дочірними елементами, переданого елементу
//   beforeend - вставити елемент після дочірніх елементів, переданих елементів
//   afterend - вставити елемент пілся переданого елементу.
//
// Як можна видалити елемент зі сторінки?
//
//   .remove() або як варіант дісплей хайд через


let myData = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];

const myFunct = (data, parentEl = document.body) => {

  let myString = "<ul>";
  data.forEach((el) => {
    myString += `<li>${el}</li>`;
  })
  myString += "</ul>";
  parentEl.insertAdjacentHTML('beforeend', myString)
}

// test 1 with default parametr
myFunct(myData);

// test 2 with custom parametr

myFunct(myData,document.querySelector("#mylist"));
