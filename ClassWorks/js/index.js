"use strict";
//
// Написать скрипт, который создаст элемент button с текстом «Войти». При клике по
// кнопке выводить alert с сообщением: «Добро пожаловать!».
//
// Callback — это функция, которая срабатывает в ответ на событие. Совсем как в
// сервисом с функцией «перезвоните мне». Мы оставляем заявку «перезвоните мне» —
// это событие. Затем специалист на перезванивает (calls back).

let myButton = document.createElement("button");
myButton.innerText ="Войти"
console.log(myButton);
document.body.insertAdjacentElement("afterbegin",myButton);

myButton.addEventListener("click",()=>alert("Добро пожаловать!"))


button.addEventListener('mouseover', hoverBtn);

function hoverBtn() {
    alert("При клике по кнопке вы войдёте в систему.");
    button.removeEventListener('mouseover', hoverBtn)
}
