"use strict";


let navItem = [...document.getElementsByClassName("services-tabs-tittle")];
let contentItem = [...document.getElementsByClassName("service-content-item")];

let list = document.getElementById("tabsmenu")
list.addEventListener("click", (e, checker) => {
  //Удаляем клас актив
  navItem.forEach((item, i) => {
    if (item.classList.contains("active")) {
      item.classList.remove("active")
    }
  })
  // добовляем актив клас
  if (e.target.closest(".services-tabs-tittle")) {
    e.target.classList.add("active");

  }
  // Находим елемнт на коотром висит клас эктив
  navItem.forEach((item, i) => {
    if (item.classList.contains("active")) {
      checker = i;
    }
  })

  // Чистим от отображения
  contentItem.forEach((item, i) => {
    if (item.classList.contains("showen")) {
      item.classList.remove("showen")
    }
    contentItem[checker].classList.add("showen")
  })
})
let portMenuItem = [...document.getElementsByClassName("portfolio-menu-item")];

let listPortMenu = document.getElementById("portfoliomenu");

let portItem = [...document.getElementsByClassName("portfolio-item")];
let filterValue = "";
listPortMenu.addEventListener("click", (e, checker) => {
  //Удаляем клас актив

  portMenuItem.forEach((item, i) => {
    if (item.classList.contains("active-item")) {
      item.classList.remove("active-item")
    }
  })
  // добовляем актив клас
  if (e.target.closest(".portfolio-menu-item")) {
    e.target.classList.add("active-item");
    filterValue = e.target.getAttribute("value");

  }
  portItem.forEach((el) => {
    el.hidden = true;

    let category = el.dataset.category;
    if (filterValue == "all") {
      el.hidden = false;
    }
    if (filterValue == "web" && category == "web") {
      el.hidden = false;
    }
    if (filterValue == "graphic" && category == "graphic") {
      el.hidden = false;
    }
    if (filterValue == "landing" && category == "landing") {
      el.hidden = false;
    }
    if (filterValue == "wordpress" && category == "wordpress") {
      el.hidden = false;
    }
  })


})
let loadMoreButton = document.getElementById("loadmoreButton");
let portList = [...document.getElementsByClassName("portfolio-list")]




loadMoreButton.addEventListener("click", (event) => {

  portItem.forEach((item, i) => {
    if (item.classList.contains("hide")) {
      item.classList.remove("hide")
    }
  })
  loadMoreButton.classList.add("hide");

})
//Последняя секция
let contactData = [{
  "text": "Integer dignissim, augue tempus ultricies luctus, quam dui laoreet sem, non dictum odio nisi quis massa. Morbi pulvinar odio eget aliquam facilisis.",
  "name": "Hasan Ali",
  "profession": "UX Designer",
  "img": "https://robohash.org/3"
}, {
  "text": "Provident similique accusantium nemo autem. Veritatis obcaecati tenetur iure eius earum ut molestias architecto voluptate aliquam nihil, eveniet aliquid culpa officia aut! Impedit sit sunt quaerat, odit, tenetur error, harum nesciunt ipsum debitis quas aliquid.",
  "name": "Monu Ali",
  "profession": "UX Designer",
  "img": "https://robohash.org/4"
}, {
  "text": "Praesent at velit eget orci ullamcorper auctor sit amet quis turpis. Sed suscipit maximus augue, eget lobortis ante volutpat quis. Fusce condimentum mi a dui pulvinar, in semper neque porttitor.",
  "name": "Poni Boni",
  "profession": "UX Designer",
  "img": "https://robohash.org/6"
}, {
  "text": "Mauris semper quis ligula sit amet gravida. Donec ornare semper ornare. Fusce varius iaculis faucibus. Aliquam et magna est. Proin dignissim magna orci vulputate.",
  "name": "Moni Doni",
  "profession": "UX Designer",
  "img": "https://robohash.org/7"
}, {
  "text": "Integer dignissim, augue tempus ultricies luctus, quam dui laoreet sem, non dictum odio nisi quis massa. Morbi pulvinar odio eget aliquam facilisis.",
  "name": "Roma Uki",
  "profession": "UX Designer",
  "img": "https://robohash.org/11"
}, {
  "text": "Mauris semper quis ligula sit amet gravida. Donec ornare semper ornare. Fusce varius iaculis faucibus. Aliquam et magna est. Proin dignissim magna orci vulputate.",
  "name": "Koli koli",
  "profession": "Developer",
  "img": "https://robohash.org/13"
}]

let myCorusel = document.getElementById("carusel")
let displayedData = "";
for (var i = 0; i < 4; i++) {

  myCorusel.innerHTML += `<img class="corusel-img" src=${contactData[i].img} alt=""/>`
}


let left = document.getElementById("leftButton");
let right = document.getElementById("rightButton");
let listContats = [...document.getElementsByClassName("corusel-img")]
let dataCarusel = document.getElementById("contentdata");
//initial state carusel data block
dataCarusel.innerHTML = `<p class="data-text">${contactData[3].text}</p>`;
dataCarusel.innerHTML += `<p class="data-text-name">${contactData[3].name}</p>`;
dataCarusel.innerHTML += `<p class="data-text">${contactData[3].profession}</p>`;
dataCarusel.innerHTML += `<img class="data-text-img" src=${contactData[3].img} alt=""/>`

right.addEventListener('click', (e) => {
  contactData.unshift(contactData[contactData.length - 1]);
  contactData.pop();
  myCorusel.innerHTML = "";
  for (var i = 0; i < 4; i++) {
    if (i == 3) {
      myCorusel.innerHTML += `<img class="corusel-img leftanime" src=${contactData[i].img} alt=""/>`
      dataCarusel.innerHTML = `<p class="data-text">${contactData[3].text}</p>`;
      dataCarusel.innerHTML += `<p class="data-text-name">${contactData[3].name}</p>`;
      dataCarusel.innerHTML += `<p class="data-text">${contactData[3].profession}</p>`;
      dataCarusel.innerHTML += `<img class="data-text-img" src=${contactData[3].img} alt=""/>`
    } else {
      myCorusel.innerHTML += `<img class="corusel-img" src=${contactData[i].img} alt=""/>`
    }
  }

})

left.addEventListener('click', (e) => {

  contactData.push(contactData[0]);
  contactData.shift();
  myCorusel.innerHTML = "";
  for (var i = 0; i < 4; i++) {
    if (i == 0) {


      myCorusel.innerHTML += `<img class="corusel-img leftanime" src=${contactData[i].img} alt=""/>`;
      dataCarusel.innerHTML = `<p class="data-text">${contactData[0].text}</p>`;
      dataCarusel.innerHTML += `<p class="data-text-name">${contactData[0].name}</p>`;
      dataCarusel.innerHTML += `<p class="data-text">${contactData[0].profession}</p>`;
      dataCarusel.innerHTML += `<img class="data-text-img" src=${contactData[0].img} alt=""/>`
    } else {
      myCorusel.innerHTML += `<img class="corusel-img" src=${contactData[i].img} alt=""/>`
    }

  }

  listContats.forEach((item, i) => {
    if (item.classList.contains("leftanime")) {
      item.classList.remove("leftanime")
    }

  })


})

//Проблема, слушатель необходимо заного переназначать на список, так как список обновляется.
let caruselListner = document.getElementById("carusel");
let caruselItem = [...document.querySelectorAll(".corusel-img")]
caruselListner.addEventListener("click", (e, checker) => {
  //Удаляем клас
  caruselItem.forEach((item) => {
    if (item.classList.contains("leftanime")) {
      item.classList.remove("leftanime")
    }
  })
  // добовляем
  if (e.target.closest(".corusel-img")) {
    e.target.classList.add("leftanime");
    let imgSmallItem;
    caruselItem.forEach((item, index) => {
      if (item.classList.contains("leftanime")) {
        imgSmallItem = index;
      }
    })
    dataCarusel.innerHTML = `<p class="data-text">${contactData[imgSmallItem].text}</p>`;
    dataCarusel.innerHTML += `<p class="data-text-name">${contactData[imgSmallItem].name}</p>`;
    dataCarusel.innerHTML += `<p class="data-text">${contactData[imgSmallItem].profession}</p>`;
    dataCarusel.innerHTML += `<img class="data-text-img" src=${contactData[imgSmallItem].img} alt=""/>`
  }

})
