function add(num1, num2) {
  num1 = num1.toString().split("").reverse().join("");
  num2 = num2.toString().split("").reverse().join("");
  if (num1.length < num2.length) {
    [num1, num2] = [num2, num1];
  }
  var returnString = "";
  for (var i = 0; i < num1.length; i++) {
    var int1 = parseInt(num1[i]);
    var int2 = parseInt(num2[i] || 0);
    returnString = (int1 + int2).toString() + returnString;
  }
  return parseInt(returnString);
}
или

function add(num1, num2) {


  num1 = num1.toString().split("").reverse().join("");
  num2 = num2.toString().split("").reverse().join("");



  if (num1.length < num2.length) {
    [num1, num2] = [num2, num1];
  }



  var returnString = "";
  for (var i = 0; i < num1.length; i++) {
    var int1 = parseInt(num1[i]);
    var int2 = parseInt(num2[i] || 0);

    returnString = (int1 + int2).toString() + returnString;
  }

  return parseInt(returnString);
}

Или

function add(a, b) {
  return a * b ? +`${add(a/10|0,b/10|0)}${a%10+b%10}` : a + b
}
Или

function add(a, b) {
  let s = ""

  while (a + b) {
    s = a % 10 + b % 10 + s
    a = a / 10 | 0
    b = b / 10 | 0
  }

  return +s
}
